<?php
    namespace Obspackage\FileUploadPackage;
    require 'vendor/autoload.php';
    
    use Aws\S3\S3Client;
    use Aws\Exception\AwsException;

    class FileUploadPackage {
        public $s3Client;
        public $bucket = '';
        public $accessKey = '';
        public $secretKey = '';
        public $awsRegion = 'ap-south-1';
        
        public function __construct(){
            $this->s3Client =  S3Client::factory(array(
                'credentials' => array(
                    'key'    => $this->accessKey,
                    'secret' => $this->secretKey,
                ),
                'region' => $this->awsRegion,
                'version' => '2006-03-01'
                ));
        }

        public function uploadFile($keyname,$filePath){
            try {
                $result = $this->s3Client->putObject(array(
                    'Bucket' => $this->bucket,
                    'Key'    => $keyname,
                    'SourceFile'   => $filePath,
                    'ACL'    => 'private',
                    'ContentType' => mime_content_type($filePath)
                ));
                print_r($result);
            } catch (S3Exception $e) {
                echo $e->getMessage() . "\n";
            }
        }

    }

    $fileUploadPackage = new FileUploadPackage();
    $keyname = 'sample.png';
    $filePath = 'image.png'; // // $filepath should be absolute path to a file on disk
    $fileUploadPackage->uploadFile($keyname,$filePath);

?>